import java.util.ArrayList;

/**
 * @author wangzhongxu
 * @since 2021/2/13 15:38
 **/

public class AVLTree<K extends Comparable<K>, V> implements Map<K, V> {
    public class Node {
        K key;
        V value;
        Node left, right;
        int height;

        public Node(K key, V value, Node left, Node right) {
            this.key = key;
            this.value = value;
            this.left = left;
            this.right = right;
            this.height = 0;
        }

        public Node() {
            this(null, null, null, null);
        }

        public Node(K key, V value) {
            this(key, value, null, null);
        }
    }

    Node root;
    int size;

    public AVLTree() {
        root = null;
        size = 0;
    }

    @Override
    public void add(K key, V value) {
        root = addNode(root, key, value);
    }

    //添加键为key，值为value的新节点 并返回根节点
    private Node addNode(Node root, K key, V value) {
        if (root == null) {
            size++;
            return new Node(key, value);
        }
        if (key.compareTo(root.key) > 0) {
            root.right = addNode(root.right, key, value);
        } else if (key.compareTo(root.key) < 0) {
            root.left = addNode(root.left, key, value);
        } else if (key.compareTo(root.key) == 0) {
            root.value = value;
        }

        //更新height
        root.height = Math.max(getHeight(root.left),getHeight(root.right)) + 1;

        //计算平衡因子(左子树和右子树的高度差) > 1 时，并不满足平衡二叉树的性质
        int balanceFactor = getBalanceFactor(root);
        if (Math.abs(balanceFactor) > 1){
            System.out.println(root.key + ":" + balanceFactor + "--unbalance:");
        }
        //平衡维护
        if (balanceFactor > 1 && getBalanceFactor(root.left) >= 0){
            return rightRotate(root);
        }

        if (balanceFactor < -1 && getBalanceFactor(root.right) <= 0){
            return leftRotate(root);
        }
        //LR的情况
//            y                             y
//          /  \                          /   \
//         x   T4      先向左旋转 (x)     z     T4
//        /  \      - - - - - - - ->   / \              ----> 形式上转化成LL的情况
//        T1  z                       x T3
//           / \                     / \
//          T2  T3                  T1  T2
        if (balanceFactor > 1 && getBalanceFactor(root.left) < 0){
            root.left = leftRotate(root.left);
            return rightRotate(root);
        }

        //RL的情况
//            y                             y
//          /  \                           /  \
//         T1   x      先向右旋转 (x)       T1   z
//             / \     - - - - - - - ->       / \       ----> 形式上转换成RR的情况
//            z   T4                         T2  x
//           / \                                / \
//          T2  T3                             T3  T4
        if (balanceFactor < -1 && getBalanceFactor(root.right) > 0){
            root.right = rightRotate(root.right);
            return leftRotate(root);
        }
        return root;
    }

    // 对节点y进行向左旋转操作，返回旋转后新的根节点x
    //    y                             x
    //  /  \                          /   \
    // T1   x      向左旋转 (y)       y     z
    //     / \   - - - - - - - ->   / \   / \
    //   T2  z                     T1 T2 T3 T4
    //      / \
    //     T3 T4
    private Node leftRotate(Node y){
        Node x = y.right;
        Node T2 = x.left;

        //向左旋转
        x.left = y;
        y.right = T2;

        //更新高度
        y.height = Math.max(getHeight(y.left),getHeight(y.right)) + 1;
        x.height = Math.max(getHeight(x.left),getHeight(x.right)) + 1;

        return x;
    }



    // 对节点y进行向右旋转操作，返回旋转后新的根节点x
    //        y                              x
    //       / \                           /   \
    //      x   T4     向右旋转 (y)        z     y
    //     / \       - - - - - - - ->    / \   / \
    //    z   T3                       T1  T2 T3 T4
    //   / \
    // T1   T2
    private Node rightRotate(Node y){
        Node x = y.left;
        Node T3 = x.right;

        //向右旋转
        x.right = y;
        y.left = T3;

        //更新高度数 只有x和y节点的高度值发生了变化
        y.height = Math.max(getHeight(y.left),getHeight(y.right)) + 1;
        x.height = Math.max(getHeight(x.left),getHeight(x.right)) + 1;
        return x;
    }


    @Override
    public boolean contains(K key) {
        return contains(root, key);
    }

    private boolean contains(Node root, K key) {
        if (root == null) {
            return false;
        }
        if (root.key.compareTo(key) < 0) {
            return contains(root.right, key);
        } else if (root.key.compareTo(key) > 0) {
            return contains(root.left, key);
        } else {
            return true;
        }
    }

    //取出最小的节点
    public Node mini() {
        return miniNode(root);
    }

    public Node miniNode(Node root) {
        if (root.left == null) {
            return root;
        }
        return miniNode(root.left);
    }

    // 删除掉以node为根的二分搜索树中的最小节点
    // 返回删除节点后新的二分搜索树的根
    public Node removeMin() {
        root = removeMin(this.root);
        return root;
    }

    private Node removeMin(Node root) {
        //TODO
        if (root.left == null) {
            size--;
            return null;
        }
        root.left = removeMin(root.left);
        return root;
    }

    // 从二分搜索树中删除键为key的节点
    public V remove(K key){

        Node node = getNode(root, key);
        if(node != null){
            root = remove(root, key);
            return node.value;
        }
        return null;
    }

    private Node remove(Node node, K key){

        if( node == null )
            return null;

        Node retNode;
        if( key.compareTo(node.key) < 0 ){
            node.left = remove(node.left , key);
            // return node;
            retNode = node;
        }
        else if(key.compareTo(node.key) > 0 ){
            node.right = remove(node.right, key);
            // return node;
            retNode = node;
        }
        else{   // key.compareTo(node.key) == 0

            // 待删除节点左子树为空的情况
            if(node.left == null){
                Node rightNode = node.right;
                node.right = null;
                size --;
                // return rightNode;
                retNode = rightNode;
            }

            // 待删除节点右子树为空的情况
            else if(node.right == null){
                Node leftNode = node.left;
                node.left = null;
                size --;
                // return leftNode;
                retNode = leftNode;
            }

            // 待删除节点左右子树均不为空的情况
            else{
                // 找到比待删除节点大的最小节点, 即待删除节点右子树的最小节点
                // 用这个节点顶替待删除节点的位置
                Node successor = miniNode(node.right);
                //successor.right = removeMin(node.right);
                successor.right = remove(node.right, successor.key);
                successor.left = node.left;

                node.left = node.right = null;

                // return successor;
                retNode = successor;
            }
        }

        if(retNode == null)
            return null;

        // 更新height
        retNode.height = 1 + Math.max(getHeight(retNode.left), getHeight(retNode.right));

        // 计算平衡因子
        int balanceFactor = getBalanceFactor(retNode);

        // 平衡维护
        // LL
        if (balanceFactor > 1 && getBalanceFactor(retNode.left) >= 0)
            return rightRotate(retNode);

        // RR
        if (balanceFactor < -1 && getBalanceFactor(retNode.right) <= 0)
            return leftRotate(retNode);

        // LR
        if (balanceFactor > 1 && getBalanceFactor(retNode.left) < 0) {
            retNode.left = leftRotate(retNode.left);
            return rightRotate(retNode);
        }

        // RL
        if (balanceFactor < -1 && getBalanceFactor(retNode.right) > 0) {
            retNode.right = rightRotate(retNode.right);
            return leftRotate(retNode);
        }

        return retNode;
    }

//    @Override
//    public V remove(K key) {
//        Node node = getNode(key);
//        if (node != null){
//            root = remove(root, key);
//            return node.value;
//        }
//        return null;
//    }

//    private Node remove(Node root, K key) {
//        if (root == null) {
//            return null;
//        }
//        Node retNode;
//        if (root.key.compareTo(key) > 0) {
//            root.left = remove(root.left, key);
//            retNode = root;
//        } else if (root.key.compareTo(key) < 0) {
//            root.right = remove(root.right, key);
//            retNode = root;
//        } else {//root.key.compareTo(key) == 0
//            if (root.left == null) {
//                Node rightNode = root.right;
//                root.right = null;
//                size--;
//                retNode = rightNode;
//            }else if (root.right == null) {
//                Node leftNode = root.left;
//                root.left = null;
//                size--;
//                retNode = leftNode;
//            }else {
//                //如果当前节点的左右节点都不为空的情况
//                Node miniNode = miniNode(root.right);   //查找当前节点右节点的最小节点
//                miniNode.left = root.left;
//                miniNode.right = remove(root.right,miniNode.key);//删除右节点删除后的新节点
//                root.left = root.right = null;
//                retNode = miniNode;
//            }
//        }
//        if (retNode == null){
//            return null;
//        }
//        //更新height
//        retNode.height = Math.max(getHeight(retNode.left),getHeight(retNode.right)) + 1;
//
//        //计算平衡因子(左子树和右子树的高度差) > 1 时，并不满足平衡二叉树的性质
//        int balanceFactor = getBalanceFactor(retNode);
////        if (Math.abs(balanceFactor) > 1){
////            System.out.println(retNode.key + ":" + balanceFactor + "--unbalance:");
////        }
//        //平衡维护
//        //LL
//        if (balanceFactor > 1 && getBalanceFactor(retNode.left) >= 0){
//            return rightRotate(retNode);
//        }
//        //RR
//        if (balanceFactor < -1 && getBalanceFactor(retNode.right) <= 0){
//            return leftRotate(retNode);
//        }
//
//        //LR
//        if (balanceFactor > 1 && getBalanceFactor(retNode.left) < 0){
//            retNode.left = leftRotate(retNode.left);
//            return rightRotate(retNode);
//        }
//        //RL
//        if (balanceFactor < -1 && getBalanceFactor(retNode.right) > 0){
//            retNode.right = rightRotate(retNode.right);
//            return leftRotate(retNode);
//        }
//
//        return retNode;
//    }

    @Override
    public V get(K key) {
        Node node = getNode(root, key);
        return node == null ? null : node.value;
    }

    public Node getNode(K key){
        return getNode(root,key);
    }

    private Node getNode(Node root, K key) {
        if (root == null) {
            return null;
        }
        if (root.key.compareTo(key) < 0) {
            return getNode(root.right, key);
        } else if (root.key.compareTo(key) > 0) {
            return getNode(root.left, key);
        } else {  //root.key.compareTo(key) == 0
            return root;
        }
    }

    @Override
    public void set(K key, V value) {
        Node node = getNode(root, key);
        if (node == null) {
            throw new RuntimeException(key + "not exits");
        } else {
            node.value = value;
        }
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }


    /********************辅助方法*****************/
    /**
     * 返回某个节点的高度
     * @return
     */
    private int getHeight(Node node){
        return node == null ? 0 : node.height;
    }

    /**
     * 返回某个节点的平衡因子
     * @return
     */
    private int getBalanceFactor(Node node){
        return getHeight(node.left) - getHeight(node.right);
    }


    private boolean isBST(){
        return isBST(root);
    }
    /**
     * 判断二叉树是否为二分搜索树
     * @param root
     * @return
     */
    private boolean isBST(Node root){
        ArrayList<K> list = new ArrayList<>();
        inOrder(root,list);
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i - 1).compareTo(list.get(i)) > 0){
                return false;
            }
        }
        return true;
    }
    /**
     * 判断二叉树是否为平衡二叉树
     * @return
     */
    public boolean isBalance(){
        return isBalance(root);
    }

    private boolean isBalance(Node root) {
        if (root == null){
            return true;
        }
        if (Math.abs(getBalanceFactor(root)) > 1){
            return false;
        }
        return isBalance(root.left) &&  isBalance(root.right);
    }

    /**
     * 中序遍历 从小到大进行排列的
     * @param root
     * @param keys 存放从小到大的数据集合
     * @return
     */
    private void inOrder(Node root,ArrayList<K> keys) {
        if (root == null){
            return;
        }
        inOrder(root.left,keys);
        keys.add(root.key);
        inOrder(root.right,keys);
    }


    public static void main(String[] args) {
        System.out.println("Pride and Prejudice");
        ArrayList<String> list = new ArrayList<>();

        if (FileOperation.readFile("/Users/zhongxuwang/Desktop/gitProject/data_structure_java/09AVLTree/pride-and-prejudice.txt",list)){
            AVLTree<String, Integer> map = new AVLTree<>();
            for (String word : list) {
                if (map.contains(word)){
                    map.set(word,map.get(word) + 1);
                }else {
                    map.add(word,1);
                }
            }

            System.out.println("Total different words: " + map.getSize());
            System.out.println("Frequency of PRIDE: " + map.get("pride"));
            System.out.println("Frequency of PREJUDICE: " + map.get("prejudice"));

            System.out.println("is BST : " + map.isBST(map.root));
            System.out.println("is Balanced : " + map.isBalance());
            System.out.println("=================================");
//            System.out.println(map.inOrder(map.root));
            for (String word : list) {
                map.remove(word);
                if (!map.isBalance() || !map.isBST()){
                    throw new RuntimeException("Error");
                }
            }

            System.out.println("over");
        }


    }
}
